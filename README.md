# Bisao Runner
Jogo de corrida 2D com o bisao da computacao desenvolvido por alunos da UFSCar - Sorocaba

Integrantes:  
* [Anderson Pinheiro Garrote](http://github.com/andersongarrote)
* [André Matheus Bariani Trava](https://github.com/andrebariani)
* [Victor Watanabe](https://github.com/victorhwmn)
* Mariane Lemos Malheiros
* Isabella Lima
